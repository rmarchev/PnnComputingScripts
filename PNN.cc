#include "src/Calibration.cc"
#include "src/StringCollector.cc"
//#include "include/Calibration.hh"

void PNN(TString sample){


    //2017B
    //if(sample.EqualTo("2017B")){
    std::vector<TString> fileNames = {"7876_Pnn_joined.root", "7878_Pnn_joined.root", "7879_Pnn_joined.root", "7880_Pnn_joined.root", "7881_Pnn_joined.root", "7883_Pnn_joined.root", "7884_Pnn_joined.root", "7885_Pnn_joined.root", "7887_Pnn_joined.root", "7897_Pnn_joined.root", "7898_Pnn_joined.root", "7899_Pnn_joined.root", "7900_Pnn_joined.root", "7902_Pnn_joined.root", "7903_Pnn_joined.root", "7904_Pnn_joined.root", "7905_Pnn_joined.root", "7906_Pnn_joined.root", "7908_Pnn_joined.root", "7909_Pnn_joined.root", "7910_Pnn_joined.root", "7911_Pnn_joined.root", "7921_Pnn_joined.root", "7923_Pnn_joined.root", "7924_Pnn_joined.root", "7927_Pnn_joined.root", "7929_Pnn_joined.root", "7930_Pnn_joined.root", "7931_Pnn_joined.root", "7939_Pnn_joined.root", "7940_Pnn_joined.root", "7941_Pnn_joined.root", "7942_Pnn_joined.root", "7943_Pnn_joined.root", "7944_Pnn_joined.root", "7945_Pnn_joined.root", "7946_Pnn_joined.root", "7948_Pnn_joined.root", "7949_Pnn_joined.root", "7951_Pnn_joined.root", "7952_Pnn_joined.root", "7953_Pnn_joined.root", "7954_Pnn_joined.root", "7955_Pnn_joined.root", "7956_Pnn_joined.root", "7966_Pnn_joined.root", "7967_Pnn_joined.root", "7968_Pnn_joined.root", "7970_Pnn_joined.root", "7971_Pnn_joined.root", "7973_Pnn_joined.root", "7978_Pnn_joined.root", "7984_Pnn_joined.root", "7989_Pnn_joined.root", "7991_Pnn_joined.root", "7998_Pnn_joined.root", "7999_Pnn_joined.root", "8001_Pnn_joined.root", "8002_Pnn_joined.root", "8005_Pnn_joined.root", "8006_Pnn_joined.root", "8007_Pnn_joined.root", "8010_Pnn_joined.root", "8011_Pnn_joined.root", "8013_Pnn_joined.root", "8014_Pnn_joined.root", "8015_Pnn_joined.root", "8016_Pnn_joined.root", "8017_Pnn_joined.root", "8018_Pnn_joined.root", "8026_Pnn_joined.root", "8027_Pnn_joined.root", "8028_Pnn_joined.root"};
    TString dir = "/home/rado/git/lxplus_work/NA62/Framework/outfiles/v267_2017B/all/";
        //}

    //2016A
    //if(sample.EqualTo("2016A")){
    //std::vector<TString> fileNames = {"6291_Pnn_joined.root", "6320_Pnn_joined.root", "6321_Pnn_joined.root", "6330_Pnn_joined.root", "6341_Pnn_joined.root", "6342_Pnn_joined.root", "6343_Pnn_joined.root", "6346_Pnn_joined.root", "6348_Pnn_joined.root", "6349_Pnn_joined.root", "6350_Pnn_joined.root", "6351_Pnn_joined.root", "6352_Pnn_joined.root", "6354_Pnn_joined.root", "6355_Pnn_joined.root", "6356_Pnn_joined.root", "6362_Pnn_joined.root", "6364_Pnn_joined.root", "6365_Pnn_joined.root", "6366_Pnn_joined.root", "6368_Pnn_joined.root", "6403_Pnn_joined.root", "6420_Pnn_joined.root", "6422_Pnn_joined.root", "6431_Pnn_joined.root", "6433_Pnn_joined.root", "6434_Pnn_joined.root", "6435_Pnn_joined.root", "6436_Pnn_joined.root", "6437_Pnn_joined.root", "6487_Pnn_joined.root", "6495_Pnn_joined.root", "6496_Pnn_joined.root", "6497_Pnn_joined.root", "6498_Pnn_joined.root", "6501_Pnn_joined.root", "6502_Pnn_joined.root", "6503_Pnn_joined.root", "6507_Pnn_joined.root", "6509_Pnn_joined.root", "6510_Pnn_joined.root", "6511_Pnn_joined.root", "6512_Pnn_joined.root", "6513_Pnn_joined.root", "6514_Pnn_joined.root", "6516_Pnn_joined.root", "6520_Pnn_joined.root", "6563_Pnn_joined.root", "6567_Pnn_joined.root", "6568_Pnn_joined.root", "6579_Pnn_joined.root", "6581_Pnn_joined.root", "6582_Pnn_joined.root", "6583_Pnn_joined.root", "6584_Pnn_joined.root", "6586_Pnn_joined.root", "6587_Pnn_joined.root", "6589_Pnn_joined.root", "6590_Pnn_joined.root", "6596_Pnn_joined.root", "6599_Pnn_joined.root", "6609_Pnn_joined.root", "6610_Pnn_joined.root", "6611_Pnn_joined.root", "6612_Pnn_joined.root", "6613_Pnn_joined.root", "6614_Pnn_joined.root", "6615_Pnn_joined.root", "6619_Pnn_joined.root", "6620_Pnn_joined.root", "6622_Pnn_joined.root", "6627_Pnn_joined.root", "6629_Pnn_joined.root", "6632_Pnn_joined.root", "6633_Pnn_joined.root", "6638_Pnn_joined.root", "6641_Pnn_joined.root", "6643_Pnn_joined.root", "6646_Pnn_joined.root", "6647_Pnn_joined.root", "6648_Pnn_joined.root", "6650_Pnn_joined.root", "6651_Pnn_joined.root", "6654_Pnn_joined.root", "6656_Pnn_joined.root", "6658_Pnn_joined.root", "6660_Pnn_joined.root", "6661_Pnn_joined.root", "6662_Pnn_joined.root", "6664_Pnn_joined.root", "6666_Pnn_joined.root", "6668_Pnn_joined.root", "6670_Pnn_joined.root", "6676_Pnn_joined.root", "6678_Pnn_joined.root", "6683_Pnn_joined.root", "6684_Pnn_joined.root", "6692_Pnn_joined.root", "6693_Pnn_joined.root", "6694_Pnn_joined.root"};
    //TString dir = "/home/rado/git/lxplus_work/NA62/Framework/outfiles/v267_2016A/all/";
        //}
    //std::vector<TString> fileNames = {"7876_Pnn_joined.root"};


    Calibration cal;
    cal.AddFileNames(dir, fileNames);
    cal.AddDir(dir);
    //cal.AddFile("/home/rado/git/lxplus_work/NA62/Framework/htcondor_scripts/v267_2017B/","7998_Pnn_joined","run7998");
    //cal.GTK_KTAG_TimeResolutionAllRuns("MainAnalysis", "OneTrack_IsGTK_dt_vs_burstid",2);
    //cal.GTK_KTAG_TimeResolutionPerBurst("MainAnalysis", "OneTrack_IsGTK_dt_vs_burstid", "7876",2);

    //cal.CHOD_KTAG_TimeResolutionPerRun("MainAnalysis", "OneTrack_stability_dtktag_vs_chodchannel",true);
//cal.GTK_KTAG_TimeResolutionPerRun("MainAnalysis", "OneTrack_IsGTK_dt_vs_burstid",2);

    //cal.PIDEfficiency("KaonTrackAnalysis", "K2pi_pid_pionefficiency_mmiss_vs_p", "K2pi_pid_mmiss_vs_p_beforePID", 0, 0);
    cal.PIDEfficiency("KaonTrackAnalysis", "Kmu2_pid_murejection_mmiss_vs_p", "Kmu2_pid_mmiss_vs_p_beforePID", 1, 0);
    fileNames.clear();
    return ;
}
