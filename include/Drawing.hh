#ifndef DRAWING_HH
#define DRAWING_HH 2

class TCanvas;
class TEfficiency;
class TString;
class TLine;
class TLatex;
class TH1;
class TH2;
class TF1;
class TGraph;
class TGaxis;
class TLegend;

class Drawing
{
private:
    static Drawing* fInstance;
    Drawing(){};
    ~Drawing(){};

public:
    static Drawing* GetInstance();

    void DrawBox(TCanvas* c, const double &x1, const double &x2, const double &y1, const double &y2, const int &col, const int &linecol, const int &style, const bool &transparent, const bool &wline);
    TLatex DrawLatexText(const double &x, const double &y, const double &angle, const TString &text);

    TH1* TH1AxisStyle(TH1* h, const TString &tx, const TString &ty, const int &mcol, const int &lcol);
    TH2* TH2AxisStyle(TH2* h, const TString &tx, const TString &ty);
    TGraph* TGraphAxisStyle(TGraph* h, TString tx, TString ty, int mcol,int lcol);
    TF1* TF1AxisStyle(TF1* h, const TString &tx, const TString &ty, const int &mcol, const int &lcol);
    TEfficiency* TEfficiencyAxisStyle(TEfficiency* h, const int &mcol, const int &lcol);
    void DrawFiducialRegion(TH2* fid);
    TLegend* TLegendStyle(TLegend* h);
    TLine* DrawLine(TCanvas* c, const double &x1, const double &x2, const double &y1, const double &y2, const int &col, const int &width, const int &style);
    TGaxis* GiveMeGaxis( TGaxis* axis , const TString &title, const int &col);
};

#endif
