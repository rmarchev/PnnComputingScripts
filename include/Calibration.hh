#ifndef CALIBRATION_H
#define CALIBRATION_H

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "TString.h"

class TH1;
class TFile;
class TCanvas;
class TObject;

class Calibration{

public:
    Calibration();
    ~Calibration(){};
    void SetPalette();
    void Clear();

    // void AddFile(const TFile* f, const TString &n){ fFiles.push_back( std::make_pair(n,(TFile*)f) );};
    void AddFile(const TFile* f, const TString &n);
    int  AddFile(const TString &dir, const TString &name, const TString &tag);
    void AddFileNames(const TString &dir,const std::vector<TString> &);
    void AddDir(const TString &dir);//{fDirs.push_back((TString) dir);};

    //Functions used for the calibration
    void GTK_KTAG_TimeResolutionPerBurst(const TString &, const TString &, const TString &, const int &);
    void GTK_KTAG_TimeResolutionPerRun(const TString &hdir, const TString &h, const bool &save);
    void GTK_KTAG_TimeResolutionAllRuns(const TString &hdir, const TString &h, const int &save);
    void CHOD_KTAG_TimeResolutionPerRun(const TString &hdir, const TString &h, const bool &save);

    //Physics performance
    // void PIDEfficiencyPerRun(const TString hdir, const TString h1, const TString h2 , const bool save);
    void PIDEfficiency(const TString &hdir, const TString &h1, const TString &h2 , const int &particle, const int &save);

private:
    std::vector<std::pair<TString,TFile*>> fFiles;
    std::vector<TString> fFileNames;
    std::vector<TString> fTags;
    std::vector<TString> fDirs;
    TString fSample;
};
#endif
