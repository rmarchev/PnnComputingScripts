#include "../include/Drawing.hh"
#include "TBox.h"
#include "TCanvas.h"
#include "TEfficiency.h"
#include "TLine.h"
#include "TLatex.h"
#include "TGraph.h"
#include "TGaxis.h"
#include "TLegend.h"
#include "TString.h"
#include "TH1.h"
#include "TF1.h"
#include "TH2.h"

Drawing* Drawing::fInstance = 0;

Drawing* Drawing::GetInstance(){

    if(fInstance == 0){
        fInstance = new Drawing();
    }

    return fInstance;
}

void Drawing::DrawBox(TCanvas* c, const double &x1, const double &x2, const  double &y1, const double &y2, const int &col, const int &linecol, const int &style, const bool &transparent, const bool &wline){

    TBox* r1 = new TBox(x1,y1,x2,y2);
    r1->SetFillColor(col);
    r1->SetFillStyle(style);
    r1->SetLineColor(linecol);
    //r1->SetLineStyle(2);

    if(!transparent){

        r1->Draw("l same");
        c->Update();
    }
    if(wline){
        Width_t w = 3;
        Style_t style = 1;
        TLine* r11 = new TLine( x1, y1, x1, y2);
        r11->SetLineColor(linecol);
        r11->SetLineWidth(w);
        r11->SetLineStyle(style);
        TLine* r12 = new TLine( x2, y1, x2, y2);
        r12->SetLineColor(linecol);
        r12->SetLineWidth(w);
        r12->SetLineStyle(style);

        TLine* r13 = new TLine( x1, y1, x2, y1);
        r13->SetLineColor(linecol);
        r13->SetLineWidth(w);
        r13->SetLineStyle(style);
        TLine* r14 = new TLine( x1, y2, x2, y2);
        r14->SetLineColor(linecol);
        r14->SetLineWidth(w);
        r14->SetLineStyle(style);

        TLine* r21 = new TLine( x1, y1, x1, y2);
        r21->SetLineColor(linecol);
        r21->SetLineWidth(w);
        r21->SetLineStyle(style);
        TLine* r22 = new TLine( x2, y1, x2, y2);
        r22->SetLineColor(linecol);
        r22->SetLineWidth(w);
        r22->SetLineStyle(style);

        TLine* r23 = new TLine( x1, y1, x2, y1);
        r23->SetLineColor(linecol);
        r23->SetLineWidth(w);
        r23->SetLineStyle(style);
        TLine* r24 = new TLine( x1, y2, x2, y2);
        r24->SetLineColor(linecol);
        r24->SetLineWidth(w);
        r24->SetLineStyle(style);

        r11->Draw(" l same");
        r12->Draw(" l same");
        r13->Draw(" l same");
        r14->Draw(" l same");

        r21->Draw(" l same");
        r22->Draw(" l same");
        r23->Draw(" l same");
        r24->Draw(" l same");

        c->Update();
    }

    return;
}

TH1* Drawing::TH1AxisStyle(TH1* h, const TString &tx, const TString &ty, const int &mcol, const int &lcol){

    h->GetXaxis()->SetLabelSize(.06);
    h->GetYaxis()->SetLabelSize(.06);
    h->GetZaxis()->SetLabelSize(.06);
    h->GetXaxis()->SetLabelFont(22);
    h->GetYaxis()->SetLabelFont(22);
    h->GetZaxis()->SetLabelFont(22);
    h->GetXaxis()->SetLabelOffset(0.02);
    h->GetYaxis()->SetLabelOffset(0.02);
    h->GetXaxis()->SetTitle(Form("%s",tx.Data()));
    h->GetYaxis()->SetTitle(Form("%s",ty.Data()));
    h->GetXaxis()->SetTitleSize(.06);
    h->GetYaxis()->SetTitleSize(.06);
    h->GetXaxis()->SetTitleFont(22);
    h->GetYaxis()->SetTitleFont(22);
    h->GetXaxis()->SetTitleOffset(1.5);
    h->GetYaxis()->SetTitleOffset(1.4);
    h->SetTitle("");
    h->SetMarkerStyle(20);
    h->SetMarkerSize(1.);
    h->SetMarkerColor(mcol);
    h->SetLineColor(lcol);
    return h;
}

TH2* Drawing::TH2AxisStyle(TH2* h, const TString &tx, const TString &ty){

    h->GetXaxis()->SetLabelSize(.06);
    h->GetYaxis()->SetLabelSize(.06);
    h->GetZaxis()->SetLabelSize(.06);
    h->GetXaxis()->SetLabelFont(22);
    h->GetYaxis()->SetLabelFont(22);
    h->GetZaxis()->SetLabelFont(22);
    h->GetXaxis()->SetLabelOffset(0.02);
    h->GetYaxis()->SetLabelOffset(0.02);
    h->GetXaxis()->SetTitle(Form("%s",tx.Data()));
    h->GetYaxis()->SetTitle(Form("%s",ty.Data()));
    h->GetXaxis()->SetTitleSize(.06);
    h->GetYaxis()->SetTitleSize(.06);
    h->GetXaxis()->SetTitleOffset(1.5);
    h->GetYaxis()->SetTitleOffset(1.4);
    h->GetXaxis()->SetTitleFont(22);
    h->GetYaxis()->SetTitleFont(22);


    h->SetTitle("");
    h->SetMarkerStyle(20);
    h->SetMarkerSize(1.);
    h->SetMarkerColor(kBlack);
    h->SetLineColor(kBlack);

    return h;
}

TGraph* Drawing::TGraphAxisStyle(TGraph* h, TString tx, TString ty, int mcol,int lcol){

    // h->SetTitle("");
    //h->SetTitleFont(22);
    h->SetMarkerStyle(20);
    h->SetMarkerSize(1.);
    h->SetMarkerColor(mcol);
    h->SetLineColor(lcol);
    h->SetLineWidth((Width_t)3);
    // h->GetXaxis()->SetLabelSize(.06);
    // h->GetYaxis()->SetLabelSize(.06);
    h->GetXaxis()->SetLabelSize(.04);
    h->GetYaxis()->SetLabelSize(.04);
    h->GetYaxis()->SetTitleOffset(1.1);
    h->GetXaxis()->SetLabelFont(22);
    h->GetYaxis()->SetLabelFont(22);
    h->GetXaxis()->SetTitle(Form("%s",tx.Data()));
    h->GetYaxis()->SetTitle(Form("%s",ty.Data()));
    // h->GetXaxis()->SetTitleSize(.06);
    // h->GetYaxis()->SetTitleSize(.06);
    h->GetXaxis()->SetTitleSize(.04);
    h->GetYaxis()->SetTitleSize(.04);
    h->GetXaxis()->SetTitleFont(22);
    h->GetYaxis()->SetTitleFont(22);



    return h;
}

TLatex Drawing::DrawLatexText(const double &x, const double &y, const double &angle, const TString &text){

    TLatex l(x,y,"");
    l.SetNDC();
    l.SetTextFont(22);
    l.SetTextColor(kBlack);
    l.SetTextSize(0.04);
    l.SetTextAngle(angle);
    l.DrawLatex(x, y, Form("%s", text.Data()));

    return l;
}

TEfficiency* Drawing::TEfficiencyAxisStyle(TEfficiency* h, const int &mcol, const int &lcol){

    h->SetTitle("");
    //h->SetTitleFont(22);
    h->SetMarkerStyle(20);
    h->SetMarkerSize(1.);
    h->SetMarkerColor(mcol);
    h->SetLineColor(lcol);
    h->SetLineWidth((Width_t)3);

    return h;
}
TF1* Drawing::TF1AxisStyle(TF1* h, const TString &tx, const TString &ty, const int &mcol, const int &lcol){

    h->GetXaxis()->SetLabelSize(.06);
    h->GetYaxis()->SetLabelSize(.06);
    h->GetZaxis()->SetLabelSize(.06);
    h->GetXaxis()->SetLabelFont(22);
    h->GetYaxis()->SetLabelFont(22);
    h->GetZaxis()->SetLabelFont(22);
    h->GetXaxis()->SetLabelOffset(0.02);
    h->GetYaxis()->SetLabelOffset(0.02);
    h->GetXaxis()->SetTitle(Form("%s",tx.Data()));
    h->GetYaxis()->SetTitle(Form("%s",ty.Data()));
    h->GetXaxis()->SetTitleSize(.06);
    h->GetYaxis()->SetTitleSize(.06);
    h->GetXaxis()->SetTitleFont(22);
    h->GetYaxis()->SetTitleFont(22);
    h->GetXaxis()->SetTitleOffset(1.5);
    h->GetYaxis()->SetTitleOffset(1.4);
    h->SetTitle("");
    //h->SetMarkerStyle(20);
    //h->SetMarkerSize(2.);
    h->SetLineWidth(6.);
    h->SetMarkerColor(mcol);
    h->SetLineColor(lcol);
    return h;
}
void Drawing::DrawFiducialRegion(TH2* fid){

    TLine* l1 = new TLine(115000,315,165000,75);
    l1->SetLineColor(2);
    l1->SetLineWidth((Width_t)4);

    TLine* l2 = new TLine(105000,780,165000,190);
    l2->SetLineColor(2);
    l2->SetLineWidth((Width_t)4);

    TLine* l3 = new TLine(165000,75,165000,190);
    l3->SetLineColor(2);
    l3->SetLineWidth((Width_t)4);

    TLine* l4 = new TLine(105000,780,115000,315);
    l4->SetLineColor(2);
    l4->SetLineWidth((Width_t)4);

    TLine* l5 = new TLine(105000,780,115000,315);
    l5->SetLineColor(2);
    l5->SetLineWidth((Width_t)4);

    l1->Draw("same");
    l2->Draw("same");
    l3->Draw("same");
    l4->Draw("same");
    l5->Draw("same");

    return;
}

TLegend* Drawing::TLegendStyle(TLegend* h){
    h->SetTextSize(0.06);
    h->SetTextFont(22);
    return h;
}


TLine* Drawing::DrawLine(TCanvas* c, const double &x1, const double &x2, const double &y1, const double &y2, const int &col, const int &width, const int &style){


    TLine* line = new TLine( x1,y1,x2,y2);
    line->SetLineColor(col);
    line->SetLineWidth((Width_t)width);
    line->SetLineStyle(style);
    line->Draw("same");

    return line;
}

TGaxis* Drawing::GiveMeGaxis( TGaxis* axis , const TString &title, const int &col){

    axis->SetTitle(Form("%s", title.Data()));

    axis->SetTitleColor(col);
    axis->SetTitleOffset(1.1);
    //axis->SetLabelSize(0.04);
    axis->SetLineColor(col);
    axis->SetLabelColor(col);
    axis->SetLabelColor(col);

    axis->SetLabelSize(.06);
    axis->SetLabelFont(22);
    axis->SetTitleFont(22);
    axis->SetTitleSize(.06);

    return axis;
}
