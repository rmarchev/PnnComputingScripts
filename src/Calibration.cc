#include "../include/Calibration.hh"
// #include "../include/Calibration.hh"
#include "CalibTools.cc"
#include "Drawing.cc"
#include "TColor.h"
#include "TGaxis.h"
#include "TStyle.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TEfficiency.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TObject.h"
#include "TObjString.h"
#include "TObjArray.h"
#include "TH1.h"
#include <algorithm>
#include <iterator>
#include <iostream>

Calibration::Calibration(){

    Clear();
    SetPalette();
}

void Calibration::Clear(){
    fFiles.clear();
    fTags.clear();
    fDirs.clear();
    fFileNames.clear();
}

void Calibration::SetPalette(){

    const Int_t NRGBs = 5;
    const Int_t NCont = 255;
    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    gStyle->SetOptStat(0);

    gStyle->SetTitleFont(22);
    gStyle->SetTitleFont(22,"X");
    gStyle->SetTitleFont(22,"Y");
    gStyle->SetLabelFont(22,"X");
    gStyle->SetLabelFont(22,"Y");
    gStyle->SetOptStat(0);
    gStyle->SetCanvasColor(10);
    gStyle->SetFillColor(10);
    gStyle->SetTitleColor(10);

    return ;
}

void Calibration::GTK_KTAG_TimeResolutionAllRuns(const TString &hdir,const TString &h, const int &save){

    TFile* plots = new TFile("/home/rado/Dropbox/Analysis_Progress/Pinunu/Code/PnnComputingScripts/Results DT GTK-KTAG/"+fSample+"_DT_GTK-KTAG.root","RECREATE");
    for(auto iTag : fTags){
        //std::cout << iTags << "\n";
        GTK_KTAG_TimeResolutionPerBurst(hdir,h,(const TString) iTag,save);
    }
    plots->Write();
    plots->Close();

    return;
}
void Calibration::GTK_KTAG_TimeResolutionPerBurst(const TString &hdir, const TString &h, const TString &tag, const int &save){

    auto it = std::find_if( fFiles.begin(), fFiles.end(), [tag](const std::pair<TString, TFile*>& element){ return element.first == tag.Data();} );

    TString Path = "/home/rado/Dropbox/Analysis_Progress/Pinunu/Code/PnnComputingScripts/Results DT GTK-KTAG/";
    TString Prefix = "DT_GTK-KTAG";

    std::cout << "Opening file ----" << it->second->GetName() << " with tag =  " << it->first  << "\n";

    TH2F* hdt2D = (TH2F*)it->second->Get(hdir+"/"+h);
    std::cout << "Taking data from histogram ---- " << hdt2D->GetName() << "\n";

    TGraphErrors* gr_mean  = new TGraphErrors(hdt2D->GetXaxis()->GetNbins());
    gr_mean->SetName(it->first+"_mean");
    gr_mean->SetTitle(it->first+"_mean");

    TGraphErrors* gr_sigma = new TGraphErrors(hdt2D->GetXaxis()->GetNbins());
    gr_sigma->SetName(it->first+"_mean");
    gr_sigma->SetTitle(it->first+"_sigma");


    for (auto iBin(0);iBin < hdt2D->GetXaxis()->GetNbins();iBin++){
        //for (auto iBin(0);iBin < 2;iBin++){
        TH1D* hdt = (TH1D*)hdt2D->ProjectionY(Form("dt_burst %d",iBin), iBin, iBin+1);
        if(hdt->Integral() < 200) {
            delete hdt;
            continue;
        }

        TF1* g    = new TF1("f","gaus(0)",-0.5,0.5);

        //Finding the largest bin of the histogram to set the proper
        //parameters for the fit
        double max=-9999;
        double imax=-1;
        for (auto jBin(0);jBin < hdt2D->GetXaxis()->GetNbins();jBin++){
            double bc =  hdt->GetBinContent(jBin);
            if(bc > max) imax = jBin;
        }

        g->SetParameter(0,hdt->Integral());
        g->SetParameter(1,hdt->GetBinCenter(imax));
        g->SetParameter(2,0.12);

        hdt->Fit("f","+","",-0.2,0.2);

        //Setting the points for the TGraphs
        gr_mean->SetPoint(iBin, (double)iBin+0.5, g->GetParameter(1));
        gr_mean->SetPointError(iBin,0,g->GetParError(1));

        gr_sigma->SetPoint(iBin,(double)iBin+0.5, g->GetParameter(2));
        gr_sigma->SetPointError(iBin,0,g->GetParError(2));

        delete hdt;
    }

    TCanvas* c_MEAN =  new TCanvas(tag+"_Mean_"+Prefix,tag+"_Mean_"+Prefix,1600,900);
    c_MEAN->SetMargin(0.15,0.03,0.1,0.08);
    gr_mean = (TGraphErrors*)Drawing::GetInstance()->TGraphAxisStyle((TGraph*)gr_mean,"#Burst", "Mean #Delta T_{GTK - KTAG}",kBlue, kBlue);

    gr_mean->Draw("AP");
    gr_mean->GetYaxis()->SetRangeUser(-0.1,0.1);
    //gr_mean->GetXaxis()->SetRangeUser(0.01,5);

    TCanvas* c_SIGMA =  new TCanvas(tag+"_Sigma_"+Prefix, tag+"_Sigma_"+Prefix,1600,900);
    c_SIGMA->SetMargin(0.15,0.03,0.1,0.08);
    gr_sigma = (TGraphErrors*)Drawing::GetInstance()->TGraphAxisStyle((TGraph*)gr_sigma,"#Burst", "Sigma #Delta T_{GTK - KTAG}",kBlue, kBlue);

    gr_sigma->Draw("AP");
    gr_sigma->GetYaxis()->SetRangeUser(0.09,0.2);
    //gr_sigma->GetXaxis()->SetRangeUser(0.01,5);

    if(save){

        if(save==2){
            c_SIGMA->Write();
            c_MEAN->Write();
        }
        if(save==3){
            c_SIGMA->Print(Path+"Sigma/"+fSample+"/"+tag+"/"+Prefix+"_sigma.png");
            c_MEAN->Print(Path+"Mean/"+fSample+"/"+tag+"/"+Prefix+"_mean.png");
        }
    }
    delete c_SIGMA;
    delete c_MEAN;

    return;
}
void Calibration::GTK_KTAG_TimeResolutionPerRun(const TString &hdir, const TString &h, const bool &save){

    TString Path = "/home/rado/Dropbox/Analysis_Progress/Pinunu/Code/PnnComputingScripts/Results DT GTK-KTAG/";
    TString Prefix = "_Run-by-Run_DT_GTK-KTAG";

    TGraphErrors* gr_mean  = new TGraphErrors(fFiles.size());
    gr_mean->SetName("Run-by-Run_mean");
    gr_mean->SetTitle("Run-by-Run_mean");

    TGraphErrors* gr_sigma = new TGraphErrors(fFiles.size());
    gr_sigma->SetName("Run-by-Run_mean");
    gr_sigma->SetTitle("Run-by-Run_sigma");

    for( size_t iFile=0; iFile < fFiles.size();iFile++){
        //for(auto iFile=0; iFile < 10;iFile++){
        TH2F* hdt2D = (TH2F*)fFiles[iFile].second->Get((TString)hdir+"/"+(TString)h);
        TString proj_name = "dt_run "+ fTags[iFile];
        TH1D* hdt = (TH1D*)hdt2D->ProjectionY(proj_name.Data());
        TF1* g    = new TF1("f","gaus(0)",-0.5,0.5);

//Finding the maximum of the histogram to set the proper parameters of the fit
        double max=-9999;
        double imax=-1;
        for (auto jBin(0);jBin < hdt2D->GetXaxis()->GetNbins();jBin++){
            double bc =  hdt->GetBinContent(jBin);
            if(bc > max) imax = jBin;
        }

        g->SetParameter(0,hdt->Integral());
        g->SetParameter(1,hdt->GetBinCenter(imax));
        g->SetParameter(2,0.12);
        hdt->Draw();
        hdt->Fit("f","+","",-0.2,0.2);

        //Setting the points for the TGraphs
        gr_mean->SetPoint(iFile, (double)fTags[iFile].Atof(), g->GetParameter(1));
        gr_mean->SetPointError(iFile,0,g->GetParError(1));

        gr_sigma->SetPoint(iFile,(double)fTags[iFile].Atof(), g->GetParameter(2));
        gr_sigma->SetPointError(iFile,0,g->GetParError(2));
        //std::cout << "File = " << fFileNames[iFile] << " Tag = " << fTags[iFile] << "\n";
    }

    TCanvas* c_MEAN =  new TCanvas(Form("mean"),Form("mean"),1600,900);
    c_MEAN->SetMargin(0.15,0.03,0.1,0.08);
    gr_mean = (TGraphErrors*)Drawing::GetInstance()->TGraphAxisStyle((TGraph*)gr_mean,"#Run", "Mean #Delta T_{GTK - KTAG}",kBlue, kBlue);

    gr_mean->Draw("AP");
    //gr_mean->GetYaxis()->SetRangeUser(,0.05);
    //gr_mean->GetXaxis()->SetRangeUser(0.01,5);

    TCanvas* c_SIGMA =  new TCanvas(Form("sigma"),Form("sigma"),1600,900);
    c_SIGMA->SetMargin(0.15,0.03,0.1,0.08);
    gr_sigma = (TGraphErrors*)Drawing::GetInstance()->TGraphAxisStyle((TGraph*)gr_sigma,"#Run", "Sigma #Delta T_{GTK - KTAG}",kBlue, kBlue);

    gr_sigma->Draw("AP");
    if(fTags[0].Atof() > 7000 && fTags[0].Atof() < 8000)
        gr_sigma->GetYaxis()->SetRangeUser(0.125,0.145);
    else if (fTags[0].Atof() < 7000) {
        gr_sigma->GetYaxis()->SetRangeUser(0.09,0.15);
        gr_sigma->GetXaxis()->SetRangeUser(6290, 6694);
    }

    if(save){
        c_SIGMA->Print(Path+fSample+"/"+fSample+Prefix+"_sigma.png");
        c_MEAN->Print(Path+fSample+"/"+fSample+Prefix+"_mean.png");
    }

    return;
}

///////////////////////////////////////////////// CHOD - KTAG resolution PER RUN /////////////////////////////////////
void Calibration::CHOD_KTAG_TimeResolutionPerRun(const TString &hdir, const TString &h, const bool &save){


    TString Path = "/home/rado/Dropbox/Analysis_Progress/Pinunu/Code/PnnComputingScripts/Results DT CHOD-KTAG/";
    TString Prefix = "_Run-by-Run_DT_CHOD-KTAG";

    TGraphErrors* gr_mean  = new TGraphErrors(fFiles.size());
    gr_mean->SetName("CHOD_KTAG_Run-by-Run_mean");
    gr_mean->SetTitle("CHOD_KTAG_Run-by-Run_mean");

    TGraphErrors* gr_sigma = new TGraphErrors(fFiles.size());
    gr_sigma->SetName("CHOD_KTAG_Run-by-Run_mean");
    gr_sigma->SetTitle("CHOD_KTAG_Run-by-Run_sigma");

    for( size_t iFile=0; iFile < fFiles.size();iFile++){
        //for(auto iFile=0; iFile < 10;iFile++){
        TH2F* hdt2D = (TH2F*)fFiles[iFile].second->Get((TString)hdir+"/"+(TString)h);
        TString proj_name = "dt_chod_ktag_run "+ fTags[iFile];
        TH1D* hdt = (TH1D*)hdt2D->ProjectionY(proj_name.Data());
        TF1* g    = new TF1("f","gaus(0)",-5,5);

//Finding the maximum of the histogram to set the proper parameters of the fit
        double max=-9999;
        double imax=-1;
        for (auto jBin(0);jBin < hdt2D->GetYaxis()->GetNbins();jBin++){
            double bc =  hdt->GetBinContent(jBin);
            if(bc > max) {
                imax = jBin;
                max = bc;
            }
        }
        g->SetParameter(0,hdt->Integral());
        g->SetParameter(1,hdt->GetBinCenter(imax));
        g->SetParameter(2,0.35);

        hdt->Draw();
        hdt->Fit("f","+","", hdt->GetBinCenter(imax) - 0.5, hdt->GetBinCenter(imax) + 0.5);

        //Setting the points for the TGraphs
        gr_mean->SetPoint(iFile, (double)fTags[iFile].Atof(), g->GetParameter(1));
        gr_mean->SetPointError(iFile,0,g->GetParError(1));

        gr_sigma->SetPoint(iFile,(double)fTags[iFile].Atof(), g->GetParameter(2));
        gr_sigma->SetPointError(iFile,0,g->GetParError(2));
        //std::cout << "File = " << fFileNames[iFile] << " Tag = " << fTags[iFile] << "\n";
    }
    TCanvas* c_MEAN =  new TCanvas(Form("mean"),Form("mean"),1600,900);
    c_MEAN->SetMargin(0.15,0.03,0.1,0.08);
    gr_mean = (TGraphErrors*)Drawing::GetInstance()->TGraphAxisStyle((TGraph*)gr_mean,"#Run", "Mean #Delta T_{CHOD - KTAG}",kBlue, kBlue);
    gr_mean->Draw("AP");

    TCanvas* c_SIGMA =  new TCanvas(Form("sigma"),Form("sigma"),1600,900);
    c_SIGMA->SetMargin(0.15,0.03,0.1,0.08);
    gr_sigma = (TGraphErrors*)Drawing::GetInstance()->TGraphAxisStyle((TGraph*)gr_sigma,"#Run", "Sigma #Delta T_{CHOD - KTAG}",kBlue, kBlue);
    gr_sigma->Draw("AP");

    //if(fTags[0].Atof() > 7000 && fTags[0].Atof() < 8000)
    //gr_sigma->GetYaxis()->SetRangeUser(0.125,0.145);
    //else
    if (fTags[0].Atof() < 7000) {
        //gr_sigma->GetYaxis()->SetRangeUser(0.105,0.125);
        gr_sigma->GetXaxis()->SetRangeUser(6290, 6699);
        gr_mean->GetXaxis()->SetRangeUser(6290, 6699);
    }

    if(save){
        c_SIGMA->Print(Path+"Sigma/"+fSample+"/"+fSample+Prefix+"_sigma.png");
        c_MEAN->Print(Path+"Mean/"+fSample+"/"+fSample+Prefix+"_mean.png");
    }

    return;
}

///////////////////////////////////////////////// PI+ IDENTIFICATION PER RUN /////////////////////////////////////
// void Calibration::PIDEfficiencyPerRun(const TString hdir, const TString h1, const TString h2 , const bool save){
void Calibration::PIDEfficiency(const TString &hdir, const TString &h1, const TString &h2 , const int &particle, const int &save){

    TCanvas* c[2];
    TString p_type;
    double mmisslow  = -0.05; //cuts for muon and electron
    double mmisshigh = 0.05; //cuts for muon and electron
    TString Path, Prefix;
    TGraphErrors* gr_pid  = new TGraphErrors(fFiles.size());

    switch(particle){

    case 0:
        p_type="Pion";
        mmisslow  =0.015;
        mmisshigh = 0.021;
        gr_pid->SetName("PID_Pion_Run-by-Run");
        gr_pid->SetTitle("PID_Pion_Run-by-Run");
        Path = "/home/rado/Dropbox/Analysis_Progress/Pinunu/Code/PnnComputingScripts/PID/Pion/";
        Prefix = "_pion_efficiency";
        Path = "/home/rado/Dropbox/Analysis_Progress/Pinunu/Code/PnnComputingScripts/PID/Pion/";
        break;
    case 1:
        p_type="Muon";
        gr_pid->SetName("PID_Muon_Run-by-Run");
        gr_pid->SetTitle("PID_Muon_Run-by-Run");
        Path = "/home/rado/Dropbox/Analysis_Progress/Pinunu/Code/PnnComputingScripts/PID/Muon/";
        Prefix = "_muon_rejection";
        break;

    case 2:
        p_type="Positron";
        break;
    default:
        return;
    }

    TFile* plots = new TFile(Path+fSample+"/"+p_type+fSample+"_PIDPerformance.root","RECREATE");
    //gDirectory->mkdir(p_type,p_type);
    //gDirectory->cd(p_type);
    //plots->Print();
    TString pass = (TString)hdir+"/"+(TString)h1;
    TString tot  = (TString)hdir+"/"+(TString)h2;
    TEfficiency* pEff = 0;

    std::cout << "===================== PRINTING " << p_type.Data() << " PARTICLE IDENTIFICATION ====================== " << "\n";
    std::cout << "Total events histo  = " << tot.Data()  << "\n";
    std::cout << "Passed events histo = " << pass.Data() << "\n";

    for( size_t iFile(0); iFile < fFiles.size();iFile ++){
        //for(auto iFile(0); iFile < 2;iFile ++){
        TH2F* h_pass2D = (TH2F*)fFiles[iFile].second->Get(pass);
        TH2F* h_tot2D  = (TH2F*)fFiles[iFile].second->Get(tot);

        TH1D* h_pass = CalibTools::GetInstance()->GetProjectionX(h_pass2D, Form("PID pass %s",fTags[iFile].Data()),mmisslow,mmisshigh);
        TH1D* h_tot  = CalibTools::GetInstance()->GetProjectionX(h_tot2D, Form("PID tot %s",fTags[iFile].Data()),mmisslow,mmisshigh);

        if(TEfficiency::CheckConsistency(*h_pass,*h_tot)) pEff = new TEfficiency( *((TH1*)h_pass->Clone()), *((TH1*)h_tot->Clone()));

        gDirectory->Delete(h_pass->GetName());
        gDirectory->Delete(h_tot->GetName());

        TCanvas* c_dummy = new TCanvas("Run"+fTags[iFile]+Prefix,"Run"+fTags[iFile]+Prefix,1600,900);
        c_dummy->SetMargin(0.08,0.03,0.15,0.08);

        pEff  = Drawing::GetInstance()->TEfficiencyAxisStyle(pEff,kBlack,kBlack);

        //Used for testing only
        //c[iFile] = new TCanvas(p_type+fTags[iFile] ,p_type+fTags[iFile],1600,900);
        //c[iFile]->SetMargin(0.08,0.03,0.15,0.08);
        pEff->Draw("AP");
        gPad->Update();
        auto painted = pEff->GetPaintedGraph();
        // painted->GetYaxis()->SetRangeUser(-1e5,1e-3);
        if(particle==1){
            painted->SetMinimum(-1e-5);
            painted->SetMaximum(1e-3);
        }

        gPad->Update();

        //arguments : x, y, Angle, text
        TLatex xl   = Drawing::GetInstance()->DrawLatexText(0.65,0.05,0,"Track Momentum [GeV/c]");
        TLatex yl   = Drawing::GetInstance()->DrawLatexText(0.03,0.7,90,p_type+" Efficiency");
        TLatex tit  = Drawing::GetInstance()->DrawLatexText(0.45,0.95,0,Form("Run %s", fTags[iFile].Data()));
        TF1* line = new TF1("eff","pol0",15,35);
        pEff->Fit(line);

        if(particle==0)
            Drawing::GetInstance()->DrawLatexText(0.4,0.5,0,Form("%s (%f.2 +- %f.2) x10^{-2}", p_type.Data(), line->GetParameter(0)*100, line->GetParError(0)*100 ));
        if(particle==1)
            Drawing::GetInstance()->DrawLatexText(0.4,0.5,0,Form("%s (%f.2 +- %f.2) x10^{-5}", p_type.Data(), line->GetParameter(0)*1e5, line->GetParError(0)*1e5 ));

        Drawing::GetInstance()->DrawLatexText(0.45,0.6,0," 15 - 35 GeV/c");

        //std::cout << line->GetParameter(0) << " +- " << line->GetParError(0) << "\n";

        //Setting the points for the TGraphs
        gr_pid->SetPoint(iFile, (double)fTags[iFile].Atof(), line->GetParameter(0));
        gr_pid->SetPointError(iFile,0,line->GetParError(0));

        if(save){
            if(save == 2)
                c_dummy->Write();
            if(save == 3)
                c_dummy->Print(Path+fSample+"/Run"+fTags[iFile]+Prefix+".png");
        }
        delete c_dummy;
    }
    TCanvas* c_PID =  new TCanvas(p_type+" pid", p_type+" pid",1600,900);
    c_PID->SetMargin(0.15,0.03,0.1,0.08);
    gr_pid = (TGraphErrors*)Drawing::GetInstance()->TGraphAxisStyle((TGraph*)gr_pid,"#Run", p_type ,kBlue, kBlue);

    gr_pid->Draw("AP");

    if(fTags[0].Atof() > 7000 && fTags[0].Atof() < 8000){
        if(particle==0) gr_pid->GetYaxis()->SetRangeUser(0.7,0.85);
        if(particle==1) gr_pid->GetYaxis()->SetRangeUser(0,3e-5);
        gr_pid->GetXaxis()->SetRangeUser(7000, 8285);
    } else if (fTags[0].Atof() < 7000) {
        gr_pid->GetYaxis()->SetRangeUser(0.09,0.15);
        gr_pid->GetXaxis()->SetRangeUser(6290, 6694);
    }

    if(save){
        if(save == 2)
            c_PID->Write();
        if(save == 3)
            c_PID->Print(Path+fSample+"/Run-by-Run"+Prefix+".png");
    }

    plots->Write();
    plots->Close();

    return;
}

void Calibration::AddFileNames(const TString &dir,const std::vector<TString> &names){

    for(auto i: names){
        TObjArray* l = i.Tokenize("_");
        TString n = ((TObjString*)l->At(0))->GetString();
        int OK = AddFile(dir,i,n);
        if(OK==1){

            if(n.Atof()<7000) fSample = "2016A";
            if(7876 < n.Atof() && n.Atof() <=8028) fSample = "2017B";
            if(8028 < n.Atof() && n.Atof() <=8300) fSample = "2017A";
            if(n.Atof() > 8300) fSample = "2018";

            fTags.push_back(n);
            fFileNames.push_back(i);
        }
    }
    return;
}
int Calibration::AddFile(const TString &dir,const TString &name,const TString &tag){

    std::cout << "Adding file : " << dir+name << "\n";
    // TFile* f = new TFile(dir+name+".root");
    TFile* f = new TFile(dir+name);
    if(f->IsZombie() || f->GetSize() < 1e6){
        std::cout << "File is Zombie! Exiting" << "\n";
        return -1;
    }
    fFiles.push_back( std::make_pair(tag,f) );
    return 1;
}

void Calibration::AddDir(const TString &dir){
    fDirs.push_back(dir);
}

void Calibration::AddFile(const TFile* f, const TString &n){
    fFiles.push_back( std::make_pair(n,(TFile*)f) );
}
